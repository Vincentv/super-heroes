SUPER-HEROES
============

Avant-propos
------------

L'objectif de cet exercice est de :
   - Ajouter coté backend le code nécessaire pour récupérer une liste de super héros ou le détail d'un superhéro
   - Ajouter coté frontend du code affichant la liste des héros, et le détail d'un héros

Les technologies imposées sont Play/Scala et Angular. Le reste (technologie, méthodologie, etc.) est à la liberté du candidat.

L'infrastructure et les services techniques des solutions Captain Dash sont codés en Scala et Javascript avec NodeJS. 
Chaque projet sur notre repository Github porte le nom d'un super héros. Par exemple [The Human Torch](http://www.superherodb.com/human-torch/10-362/)
est un projet en Node.JS qui permet d'injecter des événements sur un bus de données, comme un lance-flamme. 

Ceci permet simplement d'utiliser des noms plus sympas que DataServer ou CoreBackend pour décrire un projet ou un composant technique.

Et donc, nous vous garantissons que votre projet ne sera utilisé que dans le but d'évaluer votre capacité
à coder et à trouver une solution, pas à vendre un nouveau produit Captain Dash :-)



Etape 1
-------

Pour cet exercice, nous utiliserons l'API [ComicVine](http://www.comicvine.com/api/documentation).
Pour cela, vous aurez besoin de récupérer une clef d'API :
`http://comicvine.gamespot.com/api/`

Exemple d'une requete permettant de récupérer la liste des héros :
`curl 'http://comicvine.gamespot.com/api/characters/?api_key={MON_API_KEY}&format=json'`
et un héro :
 `curl 'http://comicvine.gamespot.com/api/character/4005-1384/?api_key={MON_API_KEY}&format=json'`
 

Etape 2
-------

   * Créer un endpoint dans l'application Play renvoyant, en JSON, la liste des héros disponibles
   * Créer un endpoint dans l'application Play prenant un id en paramètre, et renvoyant en JSON le héros correspondant à cet id
    
    
Etape 3
-------

   * Coté front, créer une application angular, qui affichera la liste des héros renvoyés par votre backend scala.
   * Un clic sur un héro amene sur une page qui affiche le détail de ce héro.

Etape 4
-------

   Nous envoyer le code !