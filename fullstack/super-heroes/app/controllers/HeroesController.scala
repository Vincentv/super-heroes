package controllers

import javax.inject.Inject

import models.{Hero, HeroSummary, ResultSet}
import play.api.libs.json.Json
import play.api.mvc._
import services.comicvine.ComicVineApi

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class HeroesController @Inject()(hApi: ComicVineApi) extends Controller {

  def index() = Action {
    Ok(views.html.index())
  }

  def getHeroes(limit: Int, offset: Int) = Action.async {
    for (
      response <- hApi.getHeroes(limit, offset);
      heroes <- Future{ response: ResultSet[HeroSummary] }
    ) yield Ok(Json.toJson(heroes))
  }

  def getHero(id: String) = Action.async {
    for (
      response <- hApi.getHero(id);
      hero <- Future{response.results.head: Hero}
    ) yield Ok(Json.toJson(hero))
  }
}
