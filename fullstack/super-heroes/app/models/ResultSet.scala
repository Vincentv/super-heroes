package models

import play.api.libs.json._

case class ResultSet[T](
                         numberOfTotalResults: Int,
                         numberOfPageResults: Int,
                         limit: Int,
                         offset: Int,
                         results: Seq[T]
                       )

object ResultSet {

  implicit def format[T: Format] = new Format[ResultSet[T]] {

    val tFormatter: Format[T] = implicitly[Format[T]]

    def reads(js: JsValue): JsResult[ResultSet[T]] = {
      JsSuccess(ResultSet[T](
        (js \ "numberOfTotalResults").as[Int],
        (js \ "numberOfPageResults").as[Int],
        (js \ "limit").as[Int],
        (js \ "offset").as[Int],
        (js \ "results").as[Seq[T]]
      ))
    }

    def writes(rs: ResultSet[T]): JsValue = {
      Json.obj(
        "numberOfTotalResults" -> JsNumber(rs.numberOfTotalResults),
        "numberOfPageResults" -> JsNumber(rs.numberOfPageResults),
        "limit" -> JsNumber(rs.limit),
        "offset" -> JsNumber(rs.offset),
        "results" -> JsArray(rs.results.map(Json.toJson(_)))
      )
    }
  }
}