package models.comicvine

import models.{HeroImages, HeroSummary}
import play.api.libs.json._
import play.api.libs.json.Reads._

case class ComicVineHeroSummary(id: Int,
                                name: String,
                                real_name: Option[String],
                                gender: Int,
                                origin: Option[ComicVineHeroOrigin],
                                image: Option[ComicVineHeroImage],
                                deck: Option[String]
                               )

object ComicVineHeroSummary {

  implicit def comicVineHeroSummaryFormatter = new Format[ComicVineHeroSummary] {

    def reads(json: JsValue): JsResult[ComicVineHeroSummary] = {
      JsSuccess(ComicVineHeroSummary(
        (json \ "id").as[Int],
        (json \ "name").as[String],
        (json \ "real_name").asOpt[String],
        (json \ "gender").as[Int],
        (json \ "origin").asOpt[ComicVineHeroOrigin],
        (json \ "image").asOpt[ComicVineHeroImage],
        (json \ "deck").asOpt[String]
      ))
    }

    def writes(h: ComicVineHeroSummary): JsValue = {
      Json.obj(
        "id" -> JsNumber(h.id),
        "name" -> JsString(h.name),
        "real_name" -> JsString(h.real_name.orNull),
        "gender" -> JsNumber(h.gender),
        "origin" -> Json.toJson(h.origin.orNull),
        "image" -> Json.toJson(h.image),
        "deck" -> JsString(h.deck.orNull)
      )
    }
  }

  implicit def comicVineHeroSummaryToHeroSummary(cvHero: ComicVineHeroSummary): HeroSummary = HeroSummary(
    cvHero.id,
    cvHero.name,
    cvHero.real_name.getOrElse(""),
    cvHero.gender,
    cvHero.origin.map(o => o.name).getOrElse(""),
    cvHero.image.map(imgs => imgs: HeroImages).getOrElse(HeroImages.generateUnknownHeroImage(cvHero.gender)),
    cvHero.deck.getOrElse("")
  )
}