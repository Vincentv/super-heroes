package models.comicvine

import models.{Hero, HeroImages}
import play.api.libs.json._
import play.api.libs.json.Reads._

case class ComicVineHero(
                          id: Int,
                          name: String,
                          real_name: Option[String],
                          gender: Int,
                          origin: Option[ComicVineHeroOrigin],
                          image: Option[ComicVineHeroImage],
                          deck: Option[String],
                          description: Option[String],
                          aliases: Option[String],
                          powers: Option[Seq[ComicVineHeroPower]],
                          teams: Option[Seq[ComicVineHeroTeam]]
                        )

object ComicVineHero {

  implicit def comicVineHeroFormatter = new Format[ComicVineHero] {

    def reads(json: JsValue): JsResult[ComicVineHero] = {
      JsSuccess(ComicVineHero(
        (json \ "id").as[Int],
        (json \ "name").as[String],
        (json \ "real_name").asOpt[String],
        (json \ "gender").as[Int],
        (json \ "origin").asOpt[ComicVineHeroOrigin],
        (json \ "image").asOpt[ComicVineHeroImage],
        (json \ "deck").asOpt[String],
        (json \ "description").asOpt[String],
        (json \ "aliases").asOpt[String],
        (json \ "powers").asOpt[Seq[ComicVineHeroPower]],
        (json \ "teams").asOpt[Seq[ComicVineHeroTeam]]
      ))
    }

    def writes(h: ComicVineHero): JsValue = {
      Json.obj(
        "id" -> JsNumber(h.id),
        "name" -> JsString(h.name),
        "real_name" -> JsString(h.real_name.orNull),
        "gender" -> JsNumber(h.gender),
        "origin" -> Json.toJson(h.origin.orNull),
        "image" -> Json.toJson(h.image),
        "deck" -> JsString(h.deck.orNull),
        "description" -> JsString(h.description.orNull),
        "aliases" -> JsString(h.aliases.orNull),
        "powers" -> JsArray(h.powers.map(_.map(p => Json.toJson(p))).getOrElse(Seq())),
        "teams" -> JsArray(h.teams.map(_.map(p => Json.toJson(p))).getOrElse(Seq()))
      )

    }
  }

  implicit def comicVineHeroToHero(cvHero: ComicVineHero): Hero = Hero(
    cvHero.id,
    cvHero.name,
    cvHero.real_name.getOrElse(""),
    cvHero.gender,
    cvHero.origin.map(o => o.name).getOrElse(""),
    cvHero.image.map(imgs => imgs: HeroImages).getOrElse(HeroImages.generateUnknownHeroImage(cvHero.gender)),
    cvHero.deck.getOrElse(""),
    cvHero.description.getOrElse(""),
    cvHero.aliases.map(a => a.split("\n").toSeq).getOrElse(Seq()),
    cvHero.powers.map(ps => ps.map(_.name)).getOrElse(Seq()),
    cvHero.teams.map(ts => ts.map(_.name)).getOrElse(Seq())
  )
}