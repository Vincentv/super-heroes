package models.comicvine

import models.{HeroSummary, ResultSet}
import play.api.libs.json.Reads._
import play.api.libs.json._


case class ComicVineResultSet[T](
                                  status_code: Int,
                                  error: String,
                                  number_of_total_results: Int,
                                  number_of_page_results: Int,
                                  limit: Int,
                                  offset: Int,
                                  results: Seq[T]
                                )

object ComicVineResultSet {
  implicit def format[T: Format] = new Format[ComicVineResultSet[T]] {
    val tFormatter: Format[T] = implicitly[Format[T]]

    def reads(js: JsValue): JsResult[ComicVineResultSet[T]] = {
      JsSuccess(ComicVineResultSet[T](
        (js \ "status_code").as[Int],
        (js \ "error").as[String],
        (js \ "number_of_total_results").as[Int],
        (js \ "number_of_page_results").as[Int],
        (js \ "limit").as[Int],
        (js \ "offset").as[Int],
        (js \ "results").asOpt[Seq[T]].getOrElse(Seq((js \ "results").as[T]))
      ))
    }

    def writes(rs: ComicVineResultSet[T]): JsValue = {
      Json.obj(
        "statusCode" -> JsNumber(rs.status_code),
        "error" -> JsString(rs.error),
        "numberOfTotalResults" -> JsNumber(rs.number_of_total_results),
        "numberOfPageResults" -> JsNumber(rs.number_of_page_results),
        "limit" -> JsNumber(rs.limit),
        "offset" -> JsNumber(rs.offset),
        "results" -> JsArray(rs.results.map(Json.toJson(_)))
      )
    }
  }

  implicit def ComicVineResultSetToResultSet[A, B](cvResultSet: ComicVineResultSet[A])(implicit elem: A => B): ResultSet[B] = {
    ResultSet(
      cvResultSet.number_of_total_results,
      cvResultSet.number_of_page_results,
      cvResultSet.limit,
      cvResultSet.offset,
      cvResultSet.results.map(r => r: B)
    )
  }
}
