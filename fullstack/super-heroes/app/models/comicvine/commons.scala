package models.comicvine

import models.HeroImages
import play.api.libs.json.{Format, Json}

case class ComicVineHeroOrigin(name: String)

object ComicVineHeroOrigin {
  implicit def comicVineHeroOriginReads = Json.reads[ComicVineHeroOrigin]

  implicit def comicVineHeroOriginWrites = Json.writes[ComicVineHeroOrigin]
}

case class ComicVineHeroImage(icon_url: String, small_url: String)

object ComicVineHeroImage {
  implicit def comicVineHeroImageReads = Json.reads[ComicVineHeroImage]

  implicit def comicVineHeroImageWrites = Json.writes[ComicVineHeroImage]

  implicit def comicVineHeroImageToHeroImage(cvHeroImage: ComicVineHeroImage): HeroImages = HeroImages(
    cvHeroImage.icon_url,
    cvHeroImage.small_url
  )
}

case class ComicVineHeroPower(name: String)

object ComicVineHeroPower {
  implicit def comicVineHeroPowerReads = Json.reads[ComicVineHeroPower]

  implicit def comicVineHeroPowerWrites = Json.writes[ComicVineHeroPower]
}

case class ComicVineHeroTeam(name: String)

object ComicVineHeroTeam {
  implicit def comicVineHeroTeamReads = Json.reads[ComicVineHeroTeam]

  implicit def comicVineHeroTeamWrites = Json.writes[ComicVineHeroTeam]
}