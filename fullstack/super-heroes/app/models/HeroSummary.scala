package models

import play.api.libs.json._

case class HeroSummary(id: Int,
                       name: String,
                       realName: String,
                       gender: Int,
                       origin: String,
                       images: HeroImages,
                       deck: String
                      )

object HeroSummary {
  implicit def heroSummaryReads = Json.reads[HeroSummary]

  implicit def heroSummaryWrites = Json.writes[HeroSummary]
}