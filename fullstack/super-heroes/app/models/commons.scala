package models

import play.api.libs.json.Json

case class HeroImages(avatarUrl: String, backgroundUrl: String)

object HeroImages {
  implicit def HeroImagesReads = Json.reads[HeroImages]

  implicit def HeroImagesWrites = Json.writes[HeroImages]

  def generateUnknownHeroImage(gender: Int = 0): HeroImages = gender match {
    case 1 => HeroImages("assets/images/superhero_male.png", "assets/images/superhero_male.png")
    case 2 => HeroImages("assets/images/superhero_female.png", "assets/images/superhero_female.png")
    case _ => HeroImages("assets/images/unknown_character.png", "assets/images/unknown_character.png")
  }
}
