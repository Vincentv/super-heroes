package models

import play.api.libs.json._

case class Hero(id: Int,
                name: String,
                realName: String,
                gender: Int,
                origin: String,
                images: HeroImages,
                deck: String,
                description: String,
                aliases: Seq[String],
                powers: Seq[String],
                teams: Seq[String]
               )

object Hero {
  implicit def heroReads = Json.reads[Hero]

  implicit def heroWrites = Json.writes[Hero]
}
