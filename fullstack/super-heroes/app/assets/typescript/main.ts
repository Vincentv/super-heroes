import { bootstrap, element } from 'angular'

import { app } from './app/app'

element(document).ready(() => bootstrap(document, [app.name]));