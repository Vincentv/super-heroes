export function coreRouteConfig ($stateProvider: ng.ui.IStateProvider, $urlRouterProvider: ng.ui.IUrlRouterProvider): void {
    $urlRouterProvider.otherwise('/heroes');

    $stateProvider.state({
        name: 'error',
        url: '/error',
        template: '<sh-error></sh-error>'
    });
}