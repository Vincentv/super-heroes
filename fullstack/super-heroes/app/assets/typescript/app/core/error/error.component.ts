export class ErrorComponent implements ng.IComponentController {
    static selector: string = "shError";
    static options: ng.IComponentOptions = {
        controller: ErrorComponent,
        templateUrl: 'assets/typescript/app/core/error/error.component.html'
    };
}