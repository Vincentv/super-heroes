import 'angular-ui-router';
import 'angular-material';
import 'angular-sanitize';

import { module } from 'angular'

import { ErrorComponent } from './error/error.component';
import { coreRouteConfig } from './core.route';

export const coreModule = module('core', [
    'ui.router',
    'ngMaterial',
    'ngSanitize'
])
    .component(ErrorComponent.selector, ErrorComponent.options)
    .config(coreRouteConfig);