export function aboutRouteConfig ($stateProvider: ng.ui.IStateProvider): void {
    $stateProvider.state({
        name: 'about',
        url: '/about',
        template: '<sh-about></sh-about>'
    });
}

aboutRouteConfig.$inject = ['$stateProvider'];
    