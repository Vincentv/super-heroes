import { module }from 'angular';

import { coreModule } from '../core/core.module';

import { AboutComponent } from './about.component';
import { aboutRouteConfig } from './about.route';

export const aboutModule = module('about', [coreModule.name])
    .component(AboutComponent.selector, AboutComponent.options)
    .config(aboutRouteConfig);