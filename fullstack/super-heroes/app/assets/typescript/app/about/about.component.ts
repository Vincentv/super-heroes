export class AboutComponent implements ng.IComponentController {
    static selector: string = "shAbout";
    static options: ng.IComponentOptions = {
        controller: AboutComponent,
        templateUrl: 'assets/typescript/app/about/about.component.html'
    };
}