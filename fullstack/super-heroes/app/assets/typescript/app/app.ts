import { module } from 'angular';

import { aboutModule } from './about/about.module';
import { coreModule } from './core/core.module';
import { heroesModule } from './heroes/heroes.module';
import { layoutModule } from './layout/layout.module';

export const app = module('app', [
    aboutModule.name,
    coreModule.name,
    heroesModule.name,
    layoutModule.name
]);