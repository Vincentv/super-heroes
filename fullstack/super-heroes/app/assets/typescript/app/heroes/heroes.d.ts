

export interface IResultSet<T>{
    numberOfTotalResults: number,
    numberOfPageResults: number,
    limit: number,
    offset: number,
    results: T[]
}

interface IBaseHero {
    id: string
    name: string
    realName: string
    gender: number
    origin: IHeroOrigin
    image: {[property: string]: string}
    deck: string
}
export interface IHero extends IBaseHero{
    description: string
    aliases: string[]
    teams: string[]
    powers: string[]
}

export interface IHeroSummary extends IBaseHero {
    id: string
}

interface IHeroOrigin {
    name: string
}
