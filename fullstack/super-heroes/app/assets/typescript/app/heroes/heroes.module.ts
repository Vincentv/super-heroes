import * as angular from 'angular'

import { coreModule } from '../core/core.module';

import { HeroesApi }            from './shared/heroes.api';
import { HeroesContainer }      from './heroes/heroes.container';
import { HeroesIterator }       from './heroes/heroes.iterator';
import { HeroSummaryComponent } from './heroes/hero-summary.component';
import { HeroContainer }        from './hero/hero.container';
import { heroesRouteConfig }    from './heroes.route';
import { GenderFilter }         from './shared/gender.filter';

export const heroesModule = angular.module('heroes', [coreModule.name])
    .component(HeroesContainer.selector, HeroesContainer.options)
    .component(HeroSummaryComponent.selector, HeroSummaryComponent.options)
    .component(HeroContainer.selector, HeroContainer.options)
    .service(HeroesApi.selector, HeroesApi)
    .service(HeroesIterator.selector, HeroesIterator)
    .filter(GenderFilter.selector, GenderFilter.filter)
    .config(heroesRouteConfig);
