export class HeroContainer implements ng.IComponentController {
    static $inject:string[] = [];

    static selector:string = "shHero";
    static options:ng.IComponentOptions = {
        controller: HeroContainer,
        templateUrl: 'assets/typescript/app/heroes/hero/hero.container.html',
        bindings: {
            hero: '<'
        }
    };
}
