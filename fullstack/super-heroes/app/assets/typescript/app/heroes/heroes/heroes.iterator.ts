import { IHeroSummary, IResultSet } from '../heroes.d';
import { HeroesApi } from '../shared/heroes.api';

export class HeroesIterator {

    static $inject: string[] = ['HeroesApi'];
    static selector: string = 'HeroesIterator';

    private api: HeroesApi;

    public loadedPages: {[property: number]: IHeroSummary[]} = {};
    public numberOfTotalResults: number = 109795; //numItems = 0;
    public numberOfPages:number = 0;
    public limit:number = 100;

    constructor(heroesApi: HeroesApi) {
        this.api = heroesApi;
    }

    public getItemAtIndex = (index:number): IHeroSummary => {
        var pageNumber = Math.floor(index / this.limit);
        var page = this.loadedPages[pageNumber];

        if (page) {
            return page[index % this.limit];
        } else if (page !== null) {
            this.fetchPage(pageNumber);
        }

        return null;
    };

    private fetchPage = (pageNumber: number) => {
        this.loadedPages[pageNumber] = null;
        const pageOffset = pageNumber * this.limit;

        this.api.getHeroes(pageOffset, this.limit)
            .then((response:IResultSet<IHeroSummary>) => {
                this.loadedPages[pageNumber] = response.results;
                this.numberOfPages = response.numberOfPageResults;
                this.numberOfTotalResults = response.numberOfTotalResults;
            })
    };

    public getLength = ():number => this.numberOfTotalResults;
}