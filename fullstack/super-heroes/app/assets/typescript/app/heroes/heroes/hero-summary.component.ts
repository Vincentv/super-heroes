export class HeroSummaryComponent implements ng.IComponentController {
    static selector: string = "shHeroSummary";
    static options: ng.IComponentOptions = {
        controller: HeroSummaryComponent,
        templateUrl: 'assets/typescript/app/heroes/heroes/hero-summary.component.html',
        bindings: {
            hero: '<'
        }
    };
}