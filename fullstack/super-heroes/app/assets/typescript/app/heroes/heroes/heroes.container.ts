import { HeroesIterator } from './heroes.iterator'

export class HeroesContainer implements ng.IComponentController {
    static $inject: string[] = ['HeroesIterator'];

    static selector: string = "shHeroes";
    static options: ng.IComponentOptions = {
        controller: HeroesContainer,
        templateUrl: 'assets/typescript/app/heroes/heroes/heroes.container.html'
    };

    public items: HeroesIterator;

    constructor(heroesIterator : HeroesIterator) {
        this.items = heroesIterator;
    }
}

