import { HeroesContainer } from './heroes/heroes.container';
import { HeroContainer } from './hero/hero.container';
import { HeroesApi } from './shared/heroes.api';
import { IHero } from './heroes';


interface heroesRoutesParams extends ng.ui.IStateParamsService {
    heroId: number
}

export function heroesRouteConfig($stateProvider: ng.ui.IStateProvider): void {
    $stateProvider
        .state({
            name: 'heroes',
            url: '/heroes',
            component: HeroesContainer.selector
        })
        .state({
            name: 'hero',
            url: '/heroes/{heroId}',
            component: HeroContainer.selector,
            resolve: {
                hero: ($stateParams: heroesRoutesParams, HeroesApi: HeroesApi): ng.IPromise<IHero> => {
                    return HeroesApi
                        .getHero($stateParams.heroId);
                }
            }
        });
}



