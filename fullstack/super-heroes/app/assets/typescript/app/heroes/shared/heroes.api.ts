import { IHero, IResultSet, IHeroSummary } from '../heroes.d';

export class HeroesApi {

    static

    $inject: string[] = ['$http', '$state'];
    static selector: string = 'HeroesApi';

    private state: ng.ui.IStateService;
    private http: ng.IHttpService;

    constructor($http: ng.IHttpService, $state: ng.ui.IStateService) {
        this.http = $http;
        this.state = $state;
    }

    getHeroes(offset: number = 0, limit: number = 100): ng.IPromise<IResultSet<IHeroSummary>> {
        return this.http
            .get('/api/heroes', {
                params: {
                    offset: offset,
                    limit: limit
                }
            })
            .then(
                (response: any) => response.data,
                this.gotoErrorPage
            );
    }

    getHero(heroId: number): ng.IPromise<IHero> {
        return this.http
            .get(`/api/heroes/${heroId}`)
            .then(
                (response: any) => response.data,
                this.gotoErrorPage
            );
    }

    private gotoErrorPage = (reason: any) => this.state.go('error')
}