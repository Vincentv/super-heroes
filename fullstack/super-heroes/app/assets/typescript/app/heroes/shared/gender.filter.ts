export class GenderFilter {

    static $inject: string[] = [];
    static selector: string = 'gender';
    static genders: string[]=  ['Other', 'Male', 'Female', 'Other'];

    static filter (): Function {
        return (value: number): string => GenderFilter.genders[value];
    }
}