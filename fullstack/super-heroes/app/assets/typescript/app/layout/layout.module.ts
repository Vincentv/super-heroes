import { module } from 'angular'

import { SuperHeroesComponent } from './super-heroes.component'

export const layoutModule = module('layout', [])
    .component(SuperHeroesComponent.selector, SuperHeroesComponent.options);