
export class SuperHeroesComponent implements ng.IComponentController {
    static selector: string = "shSuperHeroes";
    static options: ng.IComponentOptions = {
        controller: SuperHeroesComponent,
        templateUrl: 'assets/typescript/app/layout/super-heroes.component.html'
    };
}