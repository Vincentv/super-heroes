package services.comicvine

import javax.inject.Inject

import models.comicvine.{ComicVineHero, ComicVineHeroSummary, ComicVineResultSet}
import play.api.Configuration
import play.api.libs.ws.WSClient
import scala.concurrent.ExecutionContext.Implicits.global

import scala.concurrent.Future


class ComicVineApi @Inject ()(ws: WSClient, cf: Configuration){

  val rootUrl: Option[String] = cf.getString("comicvine.api.rootUrl")
  val heroesUrl: Option[String] = rootUrl.map(_.concat("characters"))
  val heroUrl: Option[String] = rootUrl.map(_.concat("character/4005-"))
  val apiKey: Option[String] = cf.getString("comicvine.api.key")

  def getHeroes(limit: Int = 100, offset: Int = 0): Future[ComicVineResultSet[ComicVineHeroSummary]] = {
    ws.url(heroesUrl.get)
      .withQueryString("api_key" -> apiKey.get)
      .withHeaders("Accept" -> "application/json")
      .withQueryString("format" -> "json")
      .withQueryString("field_list" -> "id,name,real_name,gender,origin,image,deck")
      .withQueryString("limit" -> limit.toString)
      .withQueryString("offset" -> offset.toString)
      .get()
      .map { _.json.validate[ComicVineResultSet[ComicVineHeroSummary]].get }
  }

  def getHero(id: String): Future[ComicVineResultSet[ComicVineHero]] =  {
    ws.url(heroUrl.map(_.concat(id)).get)
      .withQueryString("api_key" -> apiKey.get)
      .withHeaders("Accept" -> "application/json")
      .withQueryString("format" -> "json")
      .withQueryString("field_list" -> "id,name,real_name,gender,origin,image,deck,description,aliases,powers,teams")
      .get()
      .map { _.json.validate[ComicVineResultSet[ComicVineHero]].get }
  }
}
