

name := """super-heroes"""
version := "1.0-SNAPSHOT"
scalaVersion := "2.11.7"

lazy val root = (project in file(".")).enablePlugins(PlayScala, SbtWeb)

incOptions := incOptions.value.withNameHashing(true)
updateOptions := updateOptions.value.withCachedResolution(cachedResoluton = true)

libraryDependencies ++= {
  Seq(
    jdbc,
    cache,
    ws,
    "org.webjars.npm" % "systemjs"            % "0.19.38",

    "org.webjars.npm" % "angular"             % "1.5.8",
    "org.webjars.npm" % "angular-animate"     % "1.5.8",
    "org.webjars.npm" % "angular-aria"        % "1.5.8",
    "org.webjars.npm" % "angular-material"    % "1.1.1",
    "org.webjars.npm" % "angular-sanitize"    % "1.5.8",
    "org.webjars.npm" % "angular-ui-router"   % "1.0.0-beta.3",
    "org.webjars.npm" % "es6-promise"         % "3.3.1",
    "org.webjars.npm" % "es6-shim"            % "0.35.1",

    "org.webjars.npm" % "typescript"          % "2.0.0",
    "org.webjars.npm" % "tslint-eslint-rules" % "1.5.0",
    "org.webjars.npm" % "tslint-microsoft-contrib" % "2.0.12",
    "org.webjars.npm" % "codelyzer"           % "0.0.28"

    //test
    //  "org.webjars.npm" % "jasmine-core" % "2.4.1"
  )
}
dependencyOverrides += "org.webjars.npm" % "minimatch" % "3.0.0"

// the typescript typing information is by convention in the typings directory
// It provides ES6 implementations. This is required when compiling to ES5.
typingsFile := Some(baseDirectory.value / "typings" / "index.d.ts")

// use the webjars npm directory (target/web/node_modules ) for resolution of module imports of angular etc
resolveFromWebjarsNodeModulesDir := true

// use the combined tslint and eslint rules plus ng2 lint rules
(rulesDirectories in tslint) := Some(List(
  tslintEslintRulesDir.value,
  tslintMsContribRulesDir.value,
  ng2LintRulesDir.value
))
JsEngineKeys.engineType := JsEngineKeys.EngineType.Node

routesGenerator := InjectedRoutesGenerator


