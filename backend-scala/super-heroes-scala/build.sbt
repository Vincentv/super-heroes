name := """super-heroes-scala"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.6"

libraryDependencies ++= Seq(
  jdbc,
  cache,
  ws,
  specs2 % Test
    , "org.apache.commons" % "commons-lang3" % "3.1"
    , "commons-io" % "commons-io" % "2.4"
    , "commons-codec" % "commons-codec" % "1.7" // for new Base64 that has support for String
    ,"org.ocpsoft.prettytime" % "prettytime" % "3.2.4.Final"
)

resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"

// Play provides two styles of routers, one expects its actions to be injected, the
// other, legacy style, accesses its actions statically.
routesGenerator := InjectedRoutesGenerator
