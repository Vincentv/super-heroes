package models

case class SuperHero(id:Long, name:String, description:String, powers:Set[Power], image_medium_url:String, image_tiny_url:String)

case class Power(id:Long, name:String, description:String)

// Repository
object SuperHero{
  def all:Set[SuperHero] = ???

  def canFly:Set[SuperHero] = ???
}
