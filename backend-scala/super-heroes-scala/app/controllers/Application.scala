package controllers

import library.{GrabSuperPower, ZapActor}
import models.SuperHero
import play.api.mvc._

// This code needs a bit of clean-up
// Keep it simple. No needs to inject in this scenario
class Application extends Controller {

  def index = Action {
    implicit request =>
      // TODO create a Play form with one field to input the super hero name
      Ok(views.html.index())
  }

  def triggerDownload() = Action {
    implicit request=>
    // TODO retrieve from a form, the superhero field is a string and it is mandatory
    // This action should have 2 exits : BadRequest or Redirect

    val superhero: String = "batman"
    ZapActor.actor ! GrabSuperPower(superhero)
    Redirect(routes.Application.index()).flashing("success" -> "Ok")
  }

  def showAllCharacters() = Action {
    implicit request =>
    // TODO list or whatever you want. Should load the list from a Repository. Can be in memory
    val heroes = Set.empty[SuperHero]

    Ok(views.html.allCharacters(heroes))
  }


}
