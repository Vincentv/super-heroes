## Projet Backend-scala

### Avant-propos

Votre objectif est de fixer une application Web écrite en Play2.4, puis ensuite d'ajouter une API REST
afin de permettre d'utiliser ce service via une API REST.

L'infrastructure et les services techniques des solutions Captain Dash sont codés en Scala et Javascript avec NodeJS. 
Chaque projet sur notre repository Github porte le nom d'un super héros. Par exemple [The Human Torch](http://www.superherodb.com/human-torch/10-362/)
est un projet en Node.JS qui permet d'injecter des événements sur un bus de données, comme un lance-flamme. 

Ceci permet simplement d'utiliser des noms plus sympas que DataServer ou CoreBackend pour décrire un projet ou un composant technique.

Et donc, nous vous garantissons que votre projet ne sera utilisé que dans le but d'évaluer votre capacité
à coder et à trouver une solution, pas à vendre un nouveau produit Captain Dash :-)

### 1ere mission : améliorer le code de l'application Play2

Nous vous livrons une application Play 2 Scala de base, prête à fonctionner, mais qui nécessite votre expertise
pour améliorer le code. Pour vous aider dans un premier temps, nous avons laissé quelques TODOs dans le code. 

La première étape consiste à compléter le code de la partie Play : 

  - Reprendre le code des templates du côté vue, afin de mettre en place un formulaire Play (helper.form...)  
  - Implémenter la validation d'un formulaire simple dans le controller Application
  - Ecrire de quoi afficher une dizaine de héros sur la page allCharacters
  - Créer un Tag Play dans la partie views afin d'afficher proprement chaque Character

Une fois le code de l'application fixé, sauver et créer un premier Tag Git sur votre projet. 
  
### 2ème mission : charger des données d'une API REST externe

Vous devez maintenant trouver et charger une liste de super-héros, ainsi que leurs pouvoirs, à partir d'une API REST 
externe. Cependant, cette API est assez lente, et vous ne pouvez pas la solliciter de manière synchrone. 

Il va donc falloir mettre en oeuvre un traitement asynchrone avec un acteur Akka, le tout intégré dans votre application Play.

Pour tester, nous vous proposons un acteur de base, auquel vous pouvez ajouter d'autres messages. 
Vous mettrez une solution de cache en place, afin de stocker les Powers d'une part, et les Characters d'autre part.
Votre API et l'application Web devront par contre respecter le modèle que nous avons écrit dans le package models.

Nous utiliserons l'API du site [ComicVine](http://www.comicvine.com/api/documentation) qui permet de trouver
des super-héros, la liste de leurs pouvoirs, ainsi que leurs équipes.

A vous d'adapter le parser JSON, afin de charger correctement ces objets.

A l'issue de cette étape, votre application doit permettre de : 

  - déclencher la mise à jour d'un super-héros (par son id ou son name) ainsi que de ses super-pouvoirs associés
  - afficher la fiche d'un super-héros, ses pouvoirs et les images 
  - permettre de vider votre Repository local et de relancer le chargement des héros et des Pouvoirs
  - ne pas crasher si l'API distante ne répond pas
  

### 3ème mission : exposer une API REST 

Votre application va être utilisé par une autre équipe, pour construire une application sur iOS. Pour cela, il vous est demandé d'ajouter des services REST sur votre application Web. Afin d'optimiser les appels et l'utilisation du réseau, vous devrez mettre en place la gestion du protocole HTTP, et tirer partie des mécanismes de cache standard. 

Les end-points à implémenter doivent permettre de :
  - retourner la liste des super-héros chargés 
  - charger la fiche d'un super-héros avec ses caractéristiques et ses pouvoirs associés
  - retourner une liste paginée d'URL vers des photos/des images pour un super-héros donné

Vous êtes libre dans la définition de vos URI et l'écriture de votre API. Chaque appel HTTP doit retourner un `ETag` afin de permettre au client iOS de ne pas recharger une fiche qu'il aurait déjà chargé. Le client iOS utilisera `If-None-Match` pour vous présenter l'ETag enregistré, s'il a déjà chargé la ressource demandée. 

### OPTIONNEL 4ème mission : sauver le monde

Et si... vous trouviez un moyen de proposer une liste de super-héros à partir d'un mot clé ? 

Si nous pouvions soumettre "mock" à votre système, nous aimerions qu'il propose [Mockingbird](http://www.superherodb.com/mockingbird/10-1328/) et d'autres super-héros.

Cela va demander une solution plus compliquée, basée sur les super-pouvoirs de nos héros, et une classification entre des termes techniques, et les noms des super-héros.


## Domaine technique

Le projet sera une application Play 2.4 Scala, avec des tests et une documentation. Le code et la documentation 
seront en Anglais.


## Livrable attendu

Le projet sera livré sous la forme d'un repository privé sur Bitbucket afin que le code de votre solution ne soit
pas librement accessible. Le code doit compiler et passer les tests.
Le projet devra être déployé sur un serveur (type Amazon ou Clever-cloud) afin d'avoir une URL prête à être utilisée
pour la restitution.

# Resources

Le site ComicVine propose une API. Quelques exemples au format JSON d'appels sont stockés dans le répertoires `samples`
http://api.comicvine.com, faire une demande d'API KEY

Vous pouvez intégrer d'autres APIs comme http://developer.marvel.com/, cependant nous n'avons pas testé d'autres APIs.

# Conseils

- Ne restez pas bloqué sur un point, contactez-nous pour poser vos questions par email
- Donnez-vous 3 fois 2 heures, pas une semaine complète
- Assurez-vous bien de livrer votre projet sur un serveur, afin d'avoir une URL le jour de la restitution
- Pensez à sauvegarder votre avancement régulièrement, et à proposer un historique Git propre
- Commentez et commitez en Anglais
- Vous pouvez faire plusieurs versions, avec différentes branches/tags
- Restez simple et pragmatique
- Vous n'êtes pas obligé d'utiliser une solution de persistance (base SQL, noSQL). Une solution "in-memory" sera acceptée.

Bonne chance !
